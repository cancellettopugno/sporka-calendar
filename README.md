# The calendar

The inspiration came from Adam Sporka's thumbcalendar, an award winning compact design.

https://adam.sporka.eu
http://thumbcalendar.com

## Implementation

The code is python 3.9, in a jupyter notebook, the central numeric block is constant.
Here you'll find two different layouts. 

 * One more compact where the weeks are also fixed and the only part thats get placed depending on the year are the month labels

 * The original layout, where months are sorted

In both cases I removed a column from the central body, respective to the original design.

## Content

The folders contain some support material used to research, and screenshots of the program output.

## Usage

Run 

    jupyter-notebook sporka-calendar-0.2.ipynb

And enter the desired year when asked

## Comment

The core is made of two simple functions, one finds the first Monday of the month, the other if the year is leap. The remaining code is for determining the position of month label, where weeks start and to highlight correctly in color only the populated cells.

